<?php
/**
 * BoxProjex Utilities PHP Cli Colour Modifier is used to format 
 * CLI output for PHP scripts.
 *
 * PHP Version 5.5
 * 
 * @package BoxProjex Utilities PHP CLI Colour Modifier 
 * @author Ricky McAlister <ricky.mcalister@boxprojex.com>
 * @version 0.1.1
 * @link 
 */
namespace Boxprojex\Utils\PhpCliColour;

class PhpCliColour
{
    /**
     * Return a formatted CLI string.
     *
     * @todo Move to a separate Namespaced class.
     *
     * @param string $message The message to be output.
     * @param bool $newLine Add a new line character at the end of the string ( default true )
     * @param string | bool Prefix to be added at the start of the string ( default false )
     *
     * @return string $message
     */
    public static function output($message = '', $newLine = true, $prefix = false)
    {
        // Define the colour codes.
        $colourCodes = array(
            '<red>' => "\e[31m",
            '</red>' => "\e[0m",
            '<green>' => "\e[32m",
            '</green>' => "\e[0m",
            '<yellow>' => "\e[33m",
            '</yellow>' => "\e[0m",
            '<blue>' => "\e[34m",
            '</blue>' => "\e[0m",
            '<cyan>' => "\e[36m",
            '</cyan>' => "\e[0m"
        );

        // Format the message by replacing tags with relevant colour codes.
        foreach ($colourCodes as $tag => $colourCode) {
            $message = str_replace($tag, $colourCode, $message);
        }

        // Add ay specified prefix.
        if ($prefix !== false) {
            $message = $prefix . $message;
        }

        // Add a new line character at the end of the line.
        if ($newLine !== false) {
            $message = "{$message}\n";
        }

        // DO NOT echo the output message during PHP Unit tests.
        // @codeCoverageIgnoreStart
        echo $message;
        // @codeCoverageIgnoreEnd
            
        return $message;
    }
}
